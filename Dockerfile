FROM node:14 as base

ENV PORT 3000

WORKDIR /app/

COPY package.json /app/
COPY package-lock.json /app/

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "develop"]
