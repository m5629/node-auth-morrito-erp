# Docker alias
assemble:
	sudo docker build -t morrito-auth-server:v1 .

containers-list:
	sudo docker ps -a

containers-prune:
	sudo docker container prune

images-list:
	sudo docker images -a

images-prune:
	sudo docker image prune

remove:
	sudo docker rmi morrito-auth-server:v1

run:
	sudo docker run -it -p 3000:3000 /app/node_modules -v $(pwd):/app -v morrito-auth-server:v1

stop:
	sudo docker stop morrito-auth-server

# Compose alias
compose-build:
	sudo docker-compose up --build

compose-config:
	sudo docker-compose config

compose-down:
	sudo docker-compose down

compose-remove:
	sudo docker rmi node-auth-morrito-erp_server

compose-stop:
	sudo docker-compose stop
