// Libs
import bodyParser from "body-parser";
import cors from 'cors';
import helmet from 'helmet';
import express from 'express';
import morganLogger from 'morgan';

const app = express();

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true, parameterLimit: 50000}));
app.use(cors());

app.use(helmet());

app.use(morganLogger('dev'));

export default app;
