import {database} from "../config";

const connection = {
  user: database.user,
  host: database.host,
  port: database.port,
  database: database.name,
  password: database.password
};

export default connection
