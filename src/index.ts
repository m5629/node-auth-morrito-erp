import app from "./app";

import {port} from "./config";

import logger from "./helpers/logger";

app.listen(port, () => {
  logger.info(`⚡ [server] running on port: ${port}`)
});
