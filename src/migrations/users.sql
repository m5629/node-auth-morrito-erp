CREATE TYPE roleCode AS ENUM ('SUPERADMIN', 'ADMIN', 'CLIENT', 'EMPLOYEE');

CREATE TABLE IF NOT EXISTS role
(
  role_id roleCode NOT NULL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
  );

CREATE TABLE IF NOT EXISTS users
(
  id UUID NOT NULL PRIMARY KEY,
  role roleCode NOT NULL,
  firstName VARCHAR(30) NOT NULL,
  lastName VARCHAR(30) NOT NULL,
  email VARCHAR(254) NOT NULL UNIQUE,
  password TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
  CONSTRAINT fk_role FOREIGN KEY(role) REFERENCES role(role_id) ON DELETE SET DEFAULT
  );
